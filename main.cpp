#include <Arduino.h>

#define SENSOR 4

long currentMillis = 0;
long previousMillis = 0;
int interval = 1000;
float calibrationFactor = 1.0;
volatile uint8_t pulseCount = 0;
uint8_t pulse1Sec = 0;
float flowRate;
unsigned int flowMilliLitres = 0;
unsigned long totalMilliLitres = 0;

class Flowmeter{

};

void IRAM_ATTR pulseCounter()
{
  pulseCount++;
}

void setup()
{
  Serial.begin(115200);

  pinMode(SENSOR, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(SENSOR), pulseCounter, FALLING);
}

void loop()
{
  currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    
    pulse1Sec = pulseCount;
    pulseCount = 0;

    flowRate = ((1000.0 / (millis() - previousMillis)) * pulse1Sec) * calibrationFactor;
    previousMillis = millis();

    flowMilliLitres = (flowRate / 60) * 1000;

    totalMilliLitres += flowMilliLitres;

    Serial.print("Flow rate: ");
    Serial.print(flowRate);
    Serial.print("L/min");
    Serial.print("\t");

    Serial.print("Output Liquid Quantity: ");
    Serial.print(totalMilliLitres);
    Serial.print("mL / ");
    Serial.print(totalMilliLitres / 1000);
    Serial.println("L");
  }
}
