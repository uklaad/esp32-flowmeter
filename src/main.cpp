#include <Arduino.h>
#include <FlowMeter.h>

FlowMeter meter0(4);
void flowmeterInit()
{
  FlowMeterCalibrationParams calParams = meter0.getCalibrationParams();
  calParams.flowRateMeasurementPeriod = 250; // 250ms
  calParams.minMillisPerReading = 50;        // 5ms
  calParams.volPerFlowCount = 0.444;         // use 1 for calibration runs
  meter0.setCalibrationParams(calParams);
  meter0.begin();
}

void setup()
{
  Serial.begin(115200);
  flowmeterInit();
}

void loop()
{
  static uint64_t timer;

  uint64_t currentTime = millis();
  if (currentTime - timer > 300)
  {
    timer = currentTime;
    Serial.print("V = ");
    Serial.print(meter0.getVolume(), 1);
    Serial.print(" ml");
    Serial.print("\t:\tR = ");
    Serial.print(meter0.getFlowRate(), 2);
    Serial.println(" ml/s");
  }
}